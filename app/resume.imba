import { Page } from './components/Page'
import { Contact } from './content/Contact'
import { Products } from './content/Products'
import { Experience } from './content/Experience'
import { Employment } from './content/Employment'
import { Education } from './content/Education'
import { Header } from './components/Header'
import { Footer } from './components/Footer'
import { Print } from './components/Print'
import { Theme } from './components/Theme'

export tag Resume
	<self>
		<Page>
			<Header>
				<Contact>
				<Products>
			<div>
				<Experience>
				<Employment>
				<Education>
			<Footer>
				<Products>
		<Theme>
		<Print>