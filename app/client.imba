import 'sanitize.css'
import { Resume } from './resume'
import { store } from './store'
const { theme, setTheme } = store.getState()

global css html @print,@page m:0
global css html @font-face src: url(./assets/fonts/Roboto-Light.ttf)
global css body h:11in w:8.5in m:auto ofy:scroll
	bgc:$background-body 
	c:$text-page
	ff:'Roboto-Light',sans-serif 
	color-adjust:exact -webkit-print-color-adjust:exact 
global css a c:$text-page td:none
global css ::selection bgc:$highlight c:$highlight-text
global css ul padding-inline-start:1mm m:0
global css img d:inline-block h:1em m:1mm mt:0.5mm

tag app
	<self>
		<Resume>

setTheme(theme)
imba.mount <app>