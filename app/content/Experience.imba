import { Expertise } from '../components/Expertise'
import { Section } from '../components/Section'

css ul list-style-type:circle pl:4mm ml:0.26mm
css b tween:all 0.5s linear bgp:right bgs:200%
	bgi:linear-gradient(to left,$background-page 50%,$highlight 50%)
	@hover bgp:0

export tag Experience
	<self>
		<Section title="Professional Experience">
			<Expertise title="Programming">
				<ul>
					<li>
						<b> "Developed declarative asynchronous programs"
						" using functional programming paradigms and
						unidirectional data flow."
					<li>
						<b> "Constructed REST and GraphQL API's"
						" utilizing recursive algorithms and run-time
						reflection to process data into dynamic
						strongly-typed data structures."
					<li>
						<b> "Designed continuously integrated fullstack applications"
						" implementing user authentication and data storage."
					<li>
						<b> "Utilized Agile methodologies"
						" to develop applications from inception to deployment."
			<Expertise title="Automation">
				<ul>
					<li>
						<b> "Incrementally introduced automation"
						" in tasks pertaining to Microsoft Office
						applications through programs that progressively
						built on each other."
					<li>
						<b> "Setup an automated build system"
						" to compile operating systems for unsupported
						legacy platforms with limited or no documentation."
					<li>
						<b> "Streamlined and authored purpose-built scripts"
						" for setting up a development enviroment from
						scratch with all of the necessary software
						and configurations."
					<li>
						<b> "Automated search engine optimization"
						" using web scraping libraries and multiple API's
						to systematically detect media and generate
						relevant metadata."
					<li>
						<b> "Detected and accommodated to variance"
						" within automated tasks using custom scripts."
			<Expertise title="Project Management">
				<ul>
					<li>
						<b> "Supervised and directed"
						" multiple employees and delegated tasks in a
						work environment with strict time constraints."
					<li>
						<b> "Maximized productivity"
						" by utilizing continuous process improvement
						techniques to recognize areas of waste and
						increase work capacity."
					<li>
						<b> "Incorporated professional training"
						" to ensure that best practices were both known
						and followed and to make certain that employees
						remained within project scope."
			<Expertise title="Consulting">
				<ul>
					<li>
						<b> "Conducted research and delivered feedback"
						" to make certain that optimal industry practices
						and frameworks were leveraged."
					<li>
						<b> "Provided insight and analysis"
						" for workflow improvements by proposing and
						employing automation and technological enhancements."
					<li>
						<b> "Advised and directed"
						" multiple teams to compose and implement standardized
						operating procedures and compliance guidelines."
					<li>
						<b> "Restructured workplace enviroments"
						" to increase productivity while keeping costs
						within a predetermined budget."
			# <Expertise title="Management">
			# 	<ul>
			# 		<li>
			# 			<b> "Handled administrative tasks"
			# 			" such as opening and closing, bank deposits,
			# 			scheduling, sales, inventory management,
			# 			and shipments."
			# 		<li>
			# 			<b> "Trained and mentored new employees and managers"
			# 			", provided consultation to local stores, and
			# 			contributed to an internal corporate forum to aid
			# 			other stores internationally."
			# 		<li>
			# 			<b> "Promoted professional and personal growth"
			# 			" to increase employee retention and conducted
			# 			yearly evaluations."
			# 		<li>
			# 			<b> "Negotiated costs to secure sales"
			# 			" while ensuring minimal loss of profit in order
			# 			to continually exceed expected metrics."