import { HeaderSection } from '../components/HeaderSection'
import { Product } from '../components/Product'

export tag Products
	<self>
		<HeaderSection.products title="Front-End">
			<Product
				name="React"
				img=import('../assets/images/React.svg')
				url="https://reactjs.org/">
			<Product
				name="Svelte"
				img=import('../assets/images/Svelte.svg')
				url="https://svelte.dev/">
			<Product
				name="Vue"
				img=import('../assets/images/Vue.svg')
				url="https://vuejs.org/">
			# <Product
			# 	name="Flutter"
			# 	img=import('../assets/images/Flutter.svg')
			# 	url="https://flutter.dev/">
			<Product
				name="Redux"
				img=import('../assets/images/Redux.svg')
				url="https://redux.js.org/">
			<Product
				name="Storybook"
				img=import('../assets/images/Storybook.svg')
				url="https://storybook.js.org/">
			<Product
				name="Tailwind"
				img=import('../assets/images/Tailwind.svg')
				url="https://tailwindcss.com/">
			<Product
				name="Material"
				img=import('../assets/images/Material.svg')
				url="https://material.io/design/">
			<Product
				name="Semantic"
				img=import('../assets/images/Semantic.svg')
				url="https://semantic-ui.com/">
			<Product
				name="Bootstrap"
				img=import('../assets/images/Bootstrap.svg')
				url="https://getbootstrap.com/">
			# <Product
			# 	name="Bulma"
			# 	img=import('../assets/images/Bulma.svg')
			# 	url="https://bulma.io/">
		<HeaderSection.products title="Back-End">
			<Product
				name="Node"
				img=import('../assets/images/Node.svg')
				url="https://nodejs.org/en/">
			<Product
				name="Express"
				img=import('../assets/images/Express.svg')
				url="https://expressjs.com/">
			<Product
				name="Go"
				img=import('../assets/images/Go.svg')
				url="https://golang.org/">
			<Product
				name="Rust"
				img=import('../assets/images/Rust.svg')
				url="https://www.rust-lang.org/">
			<Product
				name="Actix"
				img=import('../assets/images/Actix.svg')
				url="https://actix.rs/">
			# <Product
			# 	name="PHP"
			# 	img=import('../assets/images/PHP.svg')
			# 	url="https://php.net/">
			# <Product
			# 	name="Laravel"
			# 	img=import('../assets/images/Laravel.svg')
			# 	url="https://laravel.com/">
			<Product
				name="Java"
				img=import('../assets/images/Java.svg')
				url="https://www.java.com/en/">
			<Product
				name="Spring"
				img=import('../assets/images/Spring.svg')
				url="https://spring.io/">
			# <Product
			# 	name="Apache"
			# 	img=import('../assets/images/Apache.svg')
			# 	url="https://www.apache.org/">
			<Product
				name="NGINX"
				img=import('../assets/images/NGINX.svg')
				url="https://www.nginx.com/">
		<HeaderSection.products title="Database">
			<Product
				name="PostgreSQL"
				img=import('../assets/images/PostgreSQL.svg')
				url="https://www.postgresql.org/">
			<Product
				name="MongoDB"
				img=import('../assets/images/MongoDB.svg')
				url="https://www.mongodb.com/">
			<Product
				name="MariaDB"
				img=import('../assets/images/MariaDB.svg')
				url="https://mariadb.org/">
			<Product
				name="MySQL"
				img=import('../assets/images/MySQL.svg')
				url="https://www.mysql.com/">
			<Product
				name="SQLite"
				img=import('../assets/images/SQLite.svg')
				url="https://www.sqlite.org/">
			<Product
				name="GraphQL"
				img=import('../assets/images/GraphQL.svg')
				url="https://graphql.org/">
		<HeaderSection.products title="DevOps">
			<Product
				name="AWS"
				img=import('../assets/images/AWS.svg')
				url="https://aws.amazon.com/">
			<Product
				name="Google Cloud"
				img=import('../assets/images/GoogleCloud.svg')
				url="https://cloud.google.com/">
			<Product
				name="Kubernetes"
				img=import('../assets/images/Kubernetes.svg')
				url="https://kubernetes.io/">
			<Product
				name="Docker"
				img=import('../assets/images/Docker.svg')
				url="https://www.docker.com/">
			<Product
				name="OpenShift"
				img=import('../assets/images/OpenShift.svg')
				url="https://www.redhat.com/en/technologies/cloud-computing/openshift">
			<Product
				name="Vercel"
				img=import('../assets/images/Vercel.svg')
				url="https://vercel.com/">
			<Product
				name="GitlabCI"
				img=import('../assets/images/GitlabCI.svg')
				url="https://about.gitlab.com/product/continuous-integration/">
			<Product
				name="Grafana"
				img=import('../assets/images/Grafana.svg')
				url="https://grafana.com/">
			<Product
				name="Kibana"
				img=import('../assets/images/Kibana.svg')
				url="https://www.elastic.co/kibana/">
			<Product
				name="Prometheus"
				img=import('../assets/images/Prometheus.svg')
				url="https://prometheus.io/">
		<HeaderSection.products title="Scripting">
			<Product
				name="Python"
				img=import('../assets/images/Python.svg')
				url="https://www.python.org/">
			<Product
				name="Bash"
				img=import('../assets/images/Bash.svg')
				url="https://www.gnu.org/software/bash/">
			<Product
				name="PowerShell"
				img=import('../assets/images/PowerShell.svg')
				url="https://docs.microsoft.com/en-us/powershell/scripting/overview?view=powershell-6">
			<Product
				name="Batch"
				img=import('../assets/images/Batch.svg')
				url="https://docs.microsoft.com/en-us/windows-server/administration/windows-commands/windows-commands">
			# <Product
			# 	name="Power Automate"
			# 	img=import('../assets/images/Power_Automate.svg')
			# 	url="https://docs.microsoft.com/en-us/power-automate/">
			# <Product
			# 	name="Visual Basic"
			# 	img=import('../assets/images/Visual_Basic.svg')
			# 	url="https://docs.microsoft.com/en-us/dotnet/visual-basic/">
			# <Product
			# 	name="AutoHotkey"
			# 	img=import('../assets/images/AutoHotkey.svg')
			# 	url="https://www.autohotkey.com/">
		<HeaderSection.products title="Design">
			<Product
				name="Photoshop"
				img=import('../assets/images/Photoshop.svg')
				url="https://www.adobe.com/products/photoshop.html">
			<Product
				name="Illustrator"
				img=import('../assets/images/Illustrator.svg')
				url="https://www.adobe.com/products/illustrator.html">
		# <HeaderSection title="Platforms">
		# 	<Product
		# 		name="Windows"
		# 		img=import('../assets/images/Windows.svg')
		# 		url="https://www.microsoft.com/en-us/windows">
		# 	<Product
		# 		name="macOS"
		# 		img=import('../assets/images/Apple.svg')
		# 		url="https://www.apple.com/macos/">
		# 	<Product
		# 		name="Linux"
		# 		img=import('../assets/images/Linux.svg')
		# 		url="https://www.linuxfoundation.org/">
		# 	<Product
		# 		name="Android"
		# 		img=import('../assets/images/Android.svg')
		# 		url="https://www.android.com/">
		# 	<Product
		# 		name="iOS"
		# 		img=import('../assets/images/iPhone.svg')
		# 		url="https://www.apple.com/ios/">