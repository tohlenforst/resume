import { Section } from '../components/Section'
import { Job } from '../components/Job'

export tag Employment
	<self>
		<Section title="Employment History">
			<Job
				title="Software Engineer"
				employer="USAA Federal Savings Bank"
				url="https://www.usaa.com/"
				logo=import('../assets/images/USAA.svg')
				start="January 2021"
				end="Present">
			<Job
				title="Software Engineer"
				employer="Arcane Software"
				url="https://arcane.software/"
				logo=import('../assets/images/ArcaneSoftware.svg')
				start="October 2018"
				end="Present">
			<Job
				title="Information Technology Intern"
				employer="USAA Federal Savings Bank"
				url="https://www.usaa.com/"
				logo=import('../assets/images/USAA.svg')
				start="May 2020"
				end="July 2020">
			<Job
				title="Security Researcher"
				employer="The University of Texas at San Antonio"
				url="https://cs.utsa.edu/"
				logo=import('../assets/images/UTSA.svg')
				start="September 2019"
				end="March 2020">
			# <Job title="Manager"
			# 	employer="UBreakiFix"
			# 	url="https://www.ubreakifix.com/"
			# 	logo=import('../assets/images/UBreakiFix.svg')
			# 	start="January 2015"
			# 	end="May 2017">		