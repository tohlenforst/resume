import { Section } from '../components/Section'
import { School } from '../components/School'

export tag Education
	<self>
		<Section title="Education">
			<School
				title="Bachelor of Science in Computer Science"
				subtitle="Western Governor's University"
				status="Class of Spring 2022"
				logo=import('../assets/images/WGU.svg')
				degree="https://www.wgu.edu/online-it-degrees/computer-science.html"
				url="https://www.wgu.edu/">
			# <School
			# 	title="Computer Science"
			# 	subtitle="The University of Texas - San Antonio"
			# 	status="Fall 2019 - Fall 2019"
			# 	logo=import('../assets/images/Rowdy.svg')
			# 	degree="https://future.utsa.edu/study/computer-science/"
			# 	url="https://www.utsa.edu/">
			# <School
			# 	title="Computer Science"
			# 	subtitle="San Antonio College"
			# 	status="Spring 2018 - Summer 2019"
			# 	logo=import('../assets/images/SAC.svg')
			# 	degree="https://www.alamo.edu/sac/academics/program-index/science-and-technology/computer-science/"
			# 	url="https://www.alamo.edu/sac/">
			# <School
			# 	title="General Studies - Science"
			# 	subtitle="Germanna Community College"
			# 	status="Fall 2013 - Spring 2015"
			# 	logo=import('../assets/images/Germanna.svg')
			# 	degree="https://www.germanna.edu/pathways/science-engineering/science/"
			# 	url="https://www.germanna.edu/">