import { HeaderSection } from '../components/HeaderSection'
import { Product } from '../components/Product'

css .name pt:2mm fs:7.76mm fw:bold tween:styles 2s ease-in-out
	c:$name 
	@hover transform:rotateY(360deg)
css .title fs:5.64mm fw:bold tween:styles 1.5s ease-in-out
	c:$job-title
	@hover transform:rotateX(360deg)

export tag Contact
	<self>
		<div.info>
			<div.name> "Tyler Ohlenforst"
			<div.title> "Software Engineer"
		<HeaderSection.contact title="">
			<Product
				name="tohlenforst@gmail.com"
				img=import('../assets/images/Email.svg')
				url="mailto:tohlenforst@gmail.com">
			<Product
				name="tohlenforst.com"
				img=import('../assets/images/Document.svg')
				url="http://tohlenforst.com/">
			<Product
				name="760.814.1177"
				img=import('../assets/images/Phone.svg')
				url="tel:1-760-814-1177">
		<HeaderSection.social title="">
			<Product
				name="linkedin.com/in/tohlenforst"
				img=import('../assets/images/Linkedln.svg')
				url="http://linkedin.com/in/tohlenforst">
			<Product
				name="gitlab.com/tohlenforst"
				img=import('../assets/images/GitlabCI.svg')
				url="http://gitlab.com/tohlenforst">