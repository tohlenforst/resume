const white = '#F6F5F5'
const whitePure = '#FFFFFF'
const black = '#444444'
const blackPure = '#000000'
const red = '#761e1a'
const yellow = '#ffff00'
const purple = '#601E9E'

const light =
	'--name': white
	'--job-title': white
	'--highlight': yellow
	'--highlight-text': blackPure
	'--background-body': white
	'--background-page': white
	'--background-header': red
	'--text-page': black
	'--text-header': white
	'--section-title-background': red
	'--section-title-text': white
	'--header-section-title': white
	'--shadow': blackPure + '10'
	'--hr': white

const usaa = {
	...light
	'--highlight': '#F8B318'
	'--background-header': '#0E2E49'
	'--section-title-background': '#0E2E49'
}

const dark =
	'--name': whitePure
	'--job-title': whitePure
	'--highlight': purple
	'--highlight-text': whitePure
	'--background-body': blackPure
	'--background-page': blackPure
	'--background-header': blackPure
	'--text-page': whitePure
	'--text-header': whitePure
	'--section-title-background': whitePure
	'--section-title-text': blackPure
	'--header-section-title': whitePure
	'--shadow': blackPure + 'f0'
	'--hr': whitePure

export const palettes = {
	light
	usaa
	dark
}