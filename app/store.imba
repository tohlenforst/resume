import { createStore } from 'zustand/vanilla'
import { persist } from 'zustand/middleware'
import { palettes } from './assets/palettes'

export const store = createStore persist
	do(set)
		theme: palettes.light
		setTheme: do(palette)
			set do()
				Object.entries(palette).forEach do(cssProperty)
					document.body.style.setProperty(...cssProperty)
				theme: palette
		toggleTheme: do()
			set do({ theme, setTheme })
				const isLight = JSON.stringify(theme) === JSON.stringify(palettes.light)
				setTheme(isLight ? palettes.dark : palettes.light)
	name: 'resume'
