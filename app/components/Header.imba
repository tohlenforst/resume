css .header w:2.5in h:100% pl:2mm pr:2mm ta:center
	bgc:$background-header
	c:$text-header 
	::selection c:$text-header

export tag Header
	<self>
		<div.header>
			<slot>