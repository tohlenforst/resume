css li d:flex ai:center p:0.5mm tween:all 0.5s ease-in-out
	@hover ts: -0.06ex 0 $text-header, 0.06ex 0 $text-header
	@hover img filter:none
css a pos:relative c:$text-header
css img mr:1mm filter:brightness(2) grayscale(100%)
	tween:filter 0.5s ease-in-out

export tag Product
	prop name\string
	prop img\ImbaAsset
	prop url\string
	<self>
		<li.product>
			<a href=url target="_blank" rel="noopener">
				<img[w:16px h:16px] src=img>
				<span.name> name