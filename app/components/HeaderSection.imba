css ul d:flex fld:row flw:wrap jc:center
css hr bd:0 h:0.26mm bgi:linear-gradient(to right,#ffffff00,$hr,#ffffff00)

export tag HeaderSection
	prop title\string
	<self>
		<div>
			<hr>
			title && <div[fw:bold c:$header-section-title]> title
			<ul>
				<slot>