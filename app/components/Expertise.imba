css .expertise p:1mm pl:2mm pr:2m
css .title d:inline-block fw:bold pl:1mm pr:1mm tween:all 0.5s linear bgp:right bgs:200%
	bgi:linear-gradient(to left,$background-page 50%,$highlight 50%)
	@hover bgp:0
css .description fs:3.53mm
	lh:5.03mm # fix for Firefox

export tag Expertise
	prop title
	<self>
		<div.expertise>
			<div.title> title
			<div.description> <slot>