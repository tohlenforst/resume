css .section p:2mm pb:0
css .title o:0.6 ta:center tween:all 0.5s ease-in-out
	bgc:$section-title-background 
	c:$section-title-text 
	@hover o:1 ts:-0.06ex 0 $section-title-text,0.06ex 0 $section-title-text

export tag Section
	prop title
	<self>
		<div.section>
			<div.title> title
			<slot>