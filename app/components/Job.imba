css .job d:flex pt:1mm pl:2mm pr:2mm
	img pos:absolute t:0 l:0 d:inline-block h:6.33mm m:1mm ml:0 mt:0 o:0 tween:styles 0.5s ease-in-out
	span tween:opacity 0.5s ease-in-out
css .header d:grid gtc:auto auto
css .left ta:left
css .right d:flex ai:center jc:flex-end js:flex-end flg:1
css .title fw:bold
css .employer pos:relative fs:10pt font-style:italic
	@hover span o:0
	@hover img o:1

export tag Job
	prop title\string
	prop employer\string
	prop url\string
	prop logo\string
	prop start\string
	prop end\string
	<self>
		<div.job>
			<div.left>
				<div.title> title
				<div.employer>
					<a href=url target="_blank">
						<span> employer
						<img src=logo alt=employer>
			<div.right>
				<span> start + " - " + end