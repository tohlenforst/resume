css .school d:grid gtc:auto auto p:1mm pl:2mm pr:2mm
	@hover img filter:none
	a pos:relative
	img mr:0 filter:brightness(2) grayscale(100%) tween:all 0.5s ease-in-out
css .left ta:left
css .right ta:right d:flex a:center jc:flex-end
css .title fw:bold
css .subtitle fs:italic fs:3.53mm d:flex fld:row flw:wrap tween:all 0.5s ease-in-out

export tag School
	prop title\string
	prop subtitle\string
	prop status\string
	prop degree\string
	prop logo\string
	prop url\string
	<self>
		<div.school>
			<div.left>
				<div.title>
					<a href=degree target="_blank" rel="noopener">
						title
				<div.subtitle>
					<a href=url target="_blank" rel="noopener">
						subtitle
						<img.white src=logo alt=subtitle>
			<div.right>
				<div.status> status