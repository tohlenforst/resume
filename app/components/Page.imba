css .page h:11in w:8.5in m:auto d:flex
	bc:$background-page
	shadow:0px 0px 25mm 10mm $shadow

export tag Page
	<self>
		<div.page>
			<slot>

